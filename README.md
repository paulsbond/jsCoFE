# CCP4 Cloud (aka jsCoFE)

Javascript framework for distributed crystallographic computations

CCP4 Cloud is part of the CCP4 Software Suite (https://www.ccp4.ac.uk). It is provided free to academic users under the terms of the general CCP4 Licence: https://www.ccp4.ac.uk/licensing/academic_software_licence.pdf. 
