=======================================
Prepare MR Model(s) from Alignment data
=======================================

Usually, sufficiently high amino acid sequence similarity correlates well with structural similarity, which fact is used for finding suitable structure homologs from PDB or other data bases. This task uses the `HHpred alignment <https://toolkit.tuebingen.mpg.de/tools/hhpred>`_ for identifying PDB entries, suitable for making MR search models. HHpred is and advanced bioinformatics tool that can detect structural homology even when sequence similarity is at the bottom of the twilight zone and even below that. The sequence alignment generated in the search step can be used as a guide for the pruning of the homologue.

The task requires a preliminary import of a ``.hhr`` file with the results of HHpred search done at the `HHpred <https://toolkit.tuebingen.mpg.de/tools/hhpred>`_ web-site using the target sequence.

The atomic coordinates of PDB entries listed in HHpred results are automatically trimmed according to the selection ranges given in HHpred output. The results are usually modified (pruned) in order to remove parts that do not correspond to the target sequence. A few modification protocols are available:

    * **Unmodified**: atomic coordinates are not altered in any way

    * **Clip**: water molecules and ligands are removed

    * **Molrep**: modification protocol, based on the comparison of the target sequence and atomic coordinates, used by
      `Molrep <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2394799/>`_

    * **Sculptor**: modification protocols, based on the comparison of the target sequence and atomic coordinates, provided by
      ``Phaser.Sculptor`` (*cf.* `here <https://www.phaser.cimr.cam.ac.uk/index.php/Molecular_Replacement>`_)

    * **Chainsaw**: modification protocols, based on the comparison of the target sequence and atomic coordinates, provided by
      `Chainsaw <https://doi.org/10.1107/S0021889808006985>`_

    * **Polyalanine**: all residues are replaced with polyalanines

The resulting models can be used in both :doc:`Phaser <doc.task.Phaser>` and :doc:`Molrep <doc.task.Molrep>` tasks, but the outcome may be different, especially in difficult cases.

.. Note::
        the task usually produces several MR models with a range of similarity to target sequence. Although higher sequence similarity implies higher success or Molecular Replacement, this is not guaranteed.

.. Note::
        combining several models, corresponding to the same target sequence, into MR Ensemble(s) may increase chances for success. MR ensembles may be prepared from individual models using the :doc:`MR Ensembles <doc.task.Ensembler>` task




**REFERENCES**

`A Completely Reimplemented MPI Bioinformatics Toolkit with a New HHpred Server at its Core.
Zimmermann L, Stephens A, Nam SZ, Rau D, Kübler J, Lozajic M, Gabler F, Söding J, Lupas AN, Alva V. J Mol Biol. 2018 Jul 20. S0022-2836(17)30587-9. <http://www.sciencedirect.com/science/article/pii/S0022283617305879>`_

`Protein Sequence Analysis Using the MPI Bioinformatics Toolkit.
Gabler F, Nam SZ, Till S, Mirdita M, Steinegger M, Söding J, Lupas AN, Alva V. Curr Protoc Bioinformatics. 2020 Dec;72(1):e108. doi: 10.1002/cpbi.108. <https://currentprotocols.onlinelibrary.wiley.com/doi/full/10.1002/cpbi.108>`_

`Söding J. (2005) Protein homology detection by HMM-HMM comparison. Bioinformatics 21: 951-960. PMID: 15531603 <https://www.ncbi.nlm.nih.gov/pubmed/15531603>`_

`Hildebrand A., Remmert A., Biegert A., Söding J. (2009). Fast and accurate automatic structure prediction with HHpred. Proteins 77(Suppl 9):128-32. doi: 10.1002/prot.22499. PMID: 19626712 <https://www.ncbi.nlm.nih.gov/pubmed/19626712>`_

`Meier A., Söding J. (2015). Automatic Prediction of Protein 3D Structures by Probabilistic Multi-template Homology Modeling. PLoS Comput Biol. 11(10):e1004343. doi: 10.1371/journal.pcbi.1004343. PMID: 26496371 <https://www.ncbi.nlm.nih.gov/pubmed/26496371>`_
