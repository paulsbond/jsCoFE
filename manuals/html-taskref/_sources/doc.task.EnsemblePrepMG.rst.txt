===============================
Prepare MR Ensemble with CCP4mg
===============================

This task starts `CCP4mg <https://www.ccp4.ac.uk/MG/>`_ graphical viewer on your computer in a special mode, which allows you to find structural homologs to your target protein, trim and ensemble them interactively with full visual control in real-time. This task uses :doc:`MrBump <doc.task.MrBump>` software to make the :doc:`MR Ensemble <doc.task.Ensembler>` from given structural homologs.

MR Ensembles are obtained with an alignment and truncation procedure using homologues structures found in the PDB and `AFDB (AlphaFold DataBase) <https://doi.org/10.1038/s41586-021-03819-2>`__ with a sequence-based search.

Automatically generated ensembles may be further improved by manual selection of found homologs and removing unsuitable structural features. That can be especially useful in cases where homologous structures, found by `phmmer <https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002195>`__, have multiple domains and no structural alignment can adequately superpose all domains simultaneously. For such proteins, the user may wish to specify which domain to use as the search model or to use more than one domain with different structural alignments. In other instances, only distant homologues may be found and it may be necessary to use only part of the structures in MR.

It may be convenient to group, or cluster, the homologs found on the basis of their structural similarity. This can be done in CCP4mg interface with the rainbow colouring of structures displayed, using their `phmmer score <https://academic.oup.com/bioinformatics/article/21/7/951/268976>`__. The colouring scheme corresponds to that of the `HHpred <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1160169/>`_ web-application, with red indicating a high-scoring match through to blue at the lower end of the scale.


----------
Parameters
----------

**PDB sequence redundancy level** specifies the highest acceptable degree of similarity between structural models in the ensemble. The task offers a choice of 100%, 95%, 90%, 70% and 50% redundancy levels.

At 100% redundancy level, all models with identical sequences, if found, are included in the ensemble. This option can be particularly useful for a protein that can adopt a range of different conformations or in cases where minor variations in the alignment between a set of very similar structures can guide the identification of the most conserved core residues.

At redundancy level lower than 100%, only one out of two or more models with sequence similarity higher than the redundancy level, can be used. In such cases, a model from crystal with higher resolution and lower R-factor is selected as a representative of the redundant subset of models.

The 95% redundancy level provides the most broad sampling of search models, while not allowing for models with identical and nearly identical sequences in the ensemble (thus filtering out models obtained from, *e.g.,* PDB depositions corresponding to the same protein with a series of ligands bound).


**Include structures from AFDB:** this option allows to look for structural homologs, suitable for making MR search models, in `AFDB (AlphaFold DataBase) <https://doi.org/10.1038/s41586-021-03819-2>`_ (in addition to the PDB).

      **EBI-AFDB pLDDT cut-off:** AlphaFold produces a per-residue estimate of its confidence score, called **pLDDT** on a scale from 0 - 100. This option sets the minimum level of **pLDDT** of selected residues.

      * **pLDDT > 90**: residues are expected to be positioned with a high accuracy. These models should be suitable for any application that benefits from high accuracy (e.g. characterising binding sites).

      * **70 < pLDDT < 90** residues are expected to be positioned reasonably well (generally, a good backbone prediction).

      * **50 < pLDDT < 70** corresponds to residues modelled with low confidence, which should be treated with caution.

      * **pLDDT < 50** indicates residues that should not be interpreted. Such scores are a reasonably strong predictor of disorder, i.e. they suggest that the region is either unstructured in physiological conditions, or gets structured only as part of a complex.

You can search only the PDB or EBI-AFDB by unticking the relevant box in task interface.

--------
Workflow
--------

The task requires only amino acid sequence to prepare MR Ensemble with MrBump and CCP4mg.

 * homologous structures are found in PDB and/or AFDB by :doc:`MrBump <doc.task.MrBump>`, which uses `phmmer <https://www.ebi.ac.uk/Tools/hmmer/search/phmmer>`__ for scanning the corresponding sequence databases.

 * Based on the sequence alignment, search models are made from found homologs using `Sculptor <https://doi.org/10.1107/S0907444910051218>`_, which can modify the the structures in three different ways: main-chain deletion, side-chain pruning and B-factor modification.

 * Prepared MR search models are superposed using :doc:`GESAMT <doc.task.Gesamt>`, and displayed in CCP4mg.

 * Finally, MR ensemble(s) need to be composed from prepared and superposed models. This is achieved by manual selection of models and pruning the resulting ensembles on the basis of r.m.s.d. between superposed residues. The models are selected using graphical controls in the **show ranges view** window, and outlier residues are removed using the **GESAMT variance slider**.

 * once suitable ensembles are created, they should be saved back to the Cloud, using **File => Save all visible to the CCP4 Cloud** menu item of CCP4mg.

Created ensembles can be used in both :doc:`Phaser <doc.task.Phaser>` and :doc:`Molrep <doc.task.Molrep>` tasks.


**Useful links**

`CCP4mg webpage <https://www.ccp4.ac.uk/MG/>`_

`CCP4mg tutorials <https://www.ccp4.ac.uk/MG/ccp4mg_help/tutorial/index.html>`_

`CCP4mg documentation <https://www.ccp4.ac.uk/MG/ccp4mg_help/index.html>`_

`MrBUMP documentation <https://www.ccp4.ac.uk/html/mrbump.html>`_

**REFERENCES**

`Söding, J., Biegert, A., & Lupas, A. N. (2005). The HHpred interactive server for protein homology detection and structure prediction. Nucleic acids research, 33(Web Server issue), W244–W248. <https://doi.org/10.1093/nar/gki408>`_

`McNicholas, S., Potterton, E., Wilson, K.S., Noble, M.E.M. (2011) Presenting your structures: the CCP4mg molecular-graphics software. Acta Cryst. D67: 386-394 <https://doi.org/10.1107/S0907444911007281>`_

`Keegan, R.M., McNicholas, S.J., Thomas, J.M.H., Simpkin, A.J., Simkovic, F., Uski, V., Ballard, C.C., Winn, M.D., Wilson, K.S., Rigden, D.J. (2018) Recent developments in MrBUMP: better search-model preparation, graphical interaction with search models, and solution improvement and assessment. Acta Cryst. D74: 167-182 <https://doi.org/10.1107/S2059798318003455>`_

`Jumper, J., Evans, R., Pritzel, A. et al. Highly accurate protein structure prediction with AlphaFold. Nature 596, 583–589 (2021). <https://doi.org/10.1038/s41586-021-03819-2>`_
