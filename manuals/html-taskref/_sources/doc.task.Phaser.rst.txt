======
Phaser
======

.. _phaser_go:

`Phaser <https://journals.iucr.org/j/issues/2007/04/00/he5368/index.html>`_ is a program
for phasing macromolecular crystal structures by both molecular replacement
and experimental phasing methods.

You can find more about **Molecular Replacement with Phaser** 
`here <https://www.phaser.cimr.cam.ac.uk/index.php/Molecular_Replacement>`__.

**Experimental Phasing with Phaser** is described in
`this document <https://www.phaser.cimr.cam.ac.uk/index.php/Experimental_Phasing>`_.

Check top ten tips for phasing with Phaser `here <https://www.phaser.cimr.cam.ac.uk/index.php/Top_Ten_Tips>`__.

For more information, visit
`PhaserWiki <https://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`_.
