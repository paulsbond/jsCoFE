.. jscofe documentation master file, created by
   sphinx-quickstart on Sat Nov  4 16:09:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========
User Guide
==========

.. toctree::
   :numbered:
   :maxdepth: 2

   jscofe_interface
   jscofe_workflows
   jscofe_cloudrun
   jscofe_i1toCloud
   jscofe_custom_workflows
   jscofe_archive
   jscofe_CloudED
   jscofe_QnA
   jscofe_troubleshooting
   jscofe_version_log

======
Search
======

* :ref:`search`

========================
Authors and Contributors
========================

User Guide for CCP4 Cloud was prepared by:
    - *Maria Fando (CCP4, STFC, Harwell, UK)*
    - *Eugene Krissinel (CCP4, STFC, Harwell, UK)*
    

Please send questions and suggestions to: `CCP4 Cloud Reception`_ .

.. _CCP4 Cloud Reception: ccp4_cloud@listserv.stfc.ac.uk?Subject=CCP4%20Cloud%20question
