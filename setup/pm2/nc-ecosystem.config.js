module.exports = {
  apps: [{
    name: "NC",
    script: "start-nc.sh",
    exp_backoff_restart_delay: 100
  }]
}
